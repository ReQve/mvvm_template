package com.reqve.mvvm_template.base.di.scopes

import javax.inject.Scope

@Scope
annotation class ApplicationScope