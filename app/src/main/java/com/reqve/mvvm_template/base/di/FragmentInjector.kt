package com.reqve.mvvm_template.base.di

import com.reqve.mvvm_template.ui_components.post.ui.PostListFragment

object FragmentInjector {

    fun inject(postListFragment: PostListFragment) =
        ActivityInjector.get(postListFragment.requireContext()).activityComponent
            .plusPostListComponent()
            .inject(postListFragment)

}