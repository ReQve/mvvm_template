package com.reqve.mvvm_template.base.di.module

import android.content.Context
import dagger.Module

@Module
class ActivityModule(private val context: Context)