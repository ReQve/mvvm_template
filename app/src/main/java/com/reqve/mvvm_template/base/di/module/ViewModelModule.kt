package com.reqve.mvvm_template.base.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.reqve.mvvm_template.base.di.ViewModelFactory
import com.reqve.mvvm_template.base.di.ViewModelKey
import com.reqve.mvvm_template.base.di.scopes.ApplicationScope
import com.reqve.mvvm_template.ui_components.post.ui.PostListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @ApplicationScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(PostListViewModel::class)
    internal abstract fun postListViewModel(postListViewModel: PostListViewModel): ViewModel
}