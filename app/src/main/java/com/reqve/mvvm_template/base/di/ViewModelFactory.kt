package com.reqve.mvvm_template.base.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.reqve.mvvm_template.base.di.scopes.ApplicationScope
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@ApplicationScope
class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = modelClass.cast(viewModels[modelClass]?.get())
        ?: throw NoSuchElementException("${modelClass.simpleName} not found")
}