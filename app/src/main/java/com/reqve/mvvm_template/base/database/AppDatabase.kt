package com.reqve.mvvm_template.base.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.reqve.mvvm_template.base.database.dao.PostDao
import com.reqve.mvvm_template.base.database.entity.Post

@Database(entities = [Post::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}