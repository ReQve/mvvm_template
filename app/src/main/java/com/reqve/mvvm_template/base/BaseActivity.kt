package com.reqve.mvvm_template.base

import androidx.appcompat.app.AppCompatActivity
import com.reqve.mvvm_template.base.di.ActivityInjector
import com.reqve.mvvm_template.base.di.component.ActivityComponent
import com.reqve.mvvm_template.base.di.module.ActivityModule

abstract class BaseActivity : AppCompatActivity(), ActivityInjector {

    override val activityComponent: ActivityComponent by lazy {
        App.getApplicationComponent(applicationContext)
            .plusActivityComponent(ActivityModule(this))
    }
}