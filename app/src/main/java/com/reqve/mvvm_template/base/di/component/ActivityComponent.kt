package com.reqve.mvvm_template.base.di.component

import com.reqve.mvvm_template.base.di.module.ActivityModule
import com.reqve.mvvm_template.base.di.scopes.ActivityScope
import com.reqve.mvvm_template.ui_components.post.di.PostListComponent
import dagger.Subcomponent

@Subcomponent(
    modules = [
        ActivityModule::class
    ]
)
@ActivityScope
interface ActivityComponent {
    fun plusPostListComponent(): PostListComponent
}