package com.reqve.mvvm_template.base

import android.app.Application
import android.content.Context
import com.reqve.mvvm_template.base.di.component.AppComponent
import com.reqve.mvvm_template.base.di.component.DaggerAppComponent

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent

        fun getApplicationComponent(applicationContext: Context): AppComponent =
            if (applicationContext is Application) {
                if (!::appComponent.isInitialized) {
                    appComponent = DaggerAppComponent.builder()
                        .application(applicationContext)
                        .build()
                }
                appComponent
            } else {
                throw IllegalArgumentException("Given context is not application context.")
            }
    }
}