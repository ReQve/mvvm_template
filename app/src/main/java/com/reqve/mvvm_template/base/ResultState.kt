package com.reqve.mvvm_template.base

import androidx.lifecycle.MutableLiveData

sealed class ResultState<T> {
    class Progress<T> : ResultState<T>()
    data class Success<T>(val value: T) : ResultState<T>()
    data class Error<T>(val error: Throwable) : ResultState<T>()
}

fun <T> MutableLiveData<ResultState<T>>.postProgress() = postValue(ResultState.Progress<T>())
fun <T> MutableLiveData<ResultState<T>>.postSuccess(value: T) = postValue(ResultState.Success<T>(value))
fun <T> MutableLiveData<ResultState<T>>.postError(error: Throwable) = postValue(ResultState.Error<T>(error))