package com.reqve.mvvm_template.base.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.reqve.mvvm_template.base.database.entity.Post
import io.reactivex.Single

@Dao
interface PostDao {
    @Query("SELECT * FROM post")
    fun getPosts(): Single<List<Post>>

    @Insert
    fun insertAll(users: List<Post>)
}