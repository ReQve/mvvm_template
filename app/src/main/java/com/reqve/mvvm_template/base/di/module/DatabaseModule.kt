package com.reqve.mvvm_template.base.di.module

import android.content.Context
import androidx.room.Room
import com.reqve.mvvm_template.base.database.AppDatabase
import com.reqve.mvvm_template.base.database.dao.PostDao
import com.reqve.mvvm_template.base.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @Provides
    @ApplicationScope
    internal fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "posts").build()
    }

    @Provides
    @ApplicationScope
    internal fun providePostDao(database: AppDatabase): PostDao {
        return database.postDao()
    }
}