package com.reqve.mvvm_template.base.di

import android.content.Context
import com.reqve.mvvm_template.base.BaseActivity
import com.reqve.mvvm_template.base.di.component.ActivityComponent

interface ActivityInjector {

    companion object {
        fun get(context: Context) = if (context is BaseActivity) {
            context
        } else {
            throw IllegalArgumentException("Activity injector is not ${BaseActivity::class.java.simpleName} class.")
        }
    }

    val activityComponent: ActivityComponent

}