package com.reqve.mvvm_template.base.network

import com.reqve.mvvm_template.base.database.entity.Post
import io.reactivex.Single
import retrofit2.http.GET

interface PostApi {

    @GET("posts")
    fun getPosts(): Single<List<Post>>
}