package com.reqve.mvvm_template.base.di.module

import android.app.Application
import android.content.Context
import com.reqve.mvvm_template.base.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    @ApplicationScope
    internal fun provideContext(application: Application): Context {
        return application
    }
}