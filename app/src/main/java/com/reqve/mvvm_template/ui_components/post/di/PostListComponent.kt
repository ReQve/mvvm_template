package com.reqve.mvvm_template.ui_components.post.di

import com.reqve.mvvm_template.base.di.scopes.FragmentScope
import com.reqve.mvvm_template.ui_components.post.ui.PostListFragment
import dagger.Subcomponent

@Subcomponent
@FragmentScope
interface PostListComponent {
    fun inject(postListFragment: PostListFragment)
}