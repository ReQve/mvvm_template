package com.reqve.mvvm_template.ui_components.post.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.reqve.mvvm_template.R
import com.reqve.mvvm_template.base.database.entity.Post
import kotlinx.android.synthetic.main.item_post.view.*

class PostListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var postList: List<Post> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return object : RecyclerView.ViewHolder(view) {}
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = postList[position]
        with(holder.itemView) {
            postTitle.text = item.title
            postBody.text = item.body
        }
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    fun updatePostList(postList: List<Post>) {
        this.postList = postList
        notifyDataSetChanged()
    }
}