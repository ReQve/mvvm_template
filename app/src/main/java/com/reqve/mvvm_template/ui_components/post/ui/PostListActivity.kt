package com.reqve.mvvm_template.ui_components.post.ui

import android.os.Bundle
import com.reqve.mvvm_template.R
import com.reqve.mvvm_template.base.BaseActivity

class PostListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_list)
        supportFragmentManager.beginTransaction()
            .replace(R.id.frame, PostListFragment(3))
            .commit()
    }
}