package com.reqve.mvvm_template.ui_components.post.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.reqve.mvvm_template.R
import com.reqve.mvvm_template.base.ResultState
import com.reqve.mvvm_template.base.database.entity.Post
import com.reqve.mvvm_template.base.di.FragmentInjector
import com.reqve.mvvm_template.base.di.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject

class PostListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: PostListViewModel
    private var errorSnackbar: Snackbar? = null

    val postListAdapter: PostListAdapter =
        PostListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentInjector.inject(this)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initPostList()
        observeResultState()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)[PostListViewModel::class.java]
    }

    private fun observeResultState() {
        viewModel.postResult.observe(viewLifecycleOwner, Observer {
            renderResultState(it)
        })
    }

    private fun renderResultState(resultState: ResultState<List<Post>>) {
        when (resultState) {
            is ResultState.Progress -> showProgress()
            is ResultState.Success -> showPostList(resultState.value)
            is ResultState.Error -> showErrorSnackbar(resultState.error)
        }
    }

    private fun initPostList() {
        postList.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        postList.adapter = postListAdapter
    }

    private fun showProgress() {
        hideError()
        progress.isVisible = true
    }

    private fun showPostList(posts: List<Post>) {
        hideError()
        progress.isVisible = false
        postListAdapter.updatePostList(posts)
    }

    private fun showErrorSnackbar(throwable: Throwable) {
        progress.isVisible = false
        errorSnackbar = Snackbar.make(root, throwable.message.toString(), Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry) { viewModel.refresh() }
            show()
        }
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }

    companion object {
        fun newInstance() = PostListFragment()
        operator fun invoke(placebo: Int) = PostListFragment()
    }
}