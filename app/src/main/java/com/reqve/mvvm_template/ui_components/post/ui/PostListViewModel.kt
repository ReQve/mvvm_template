package com.reqve.mvvm_template.ui_components.post.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reqve.mvvm_template.base.*
import com.reqve.mvvm_template.base.database.entity.Post
import com.reqve.mvvm_template.base.database.dao.PostDao
import com.reqve.mvvm_template.base.network.PostApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostListViewModel @Inject constructor(
    val postApi: PostApi,
    val postDao: PostDao
) : ViewModel() {

    val disposables = CompositeDisposable()

    val postResult: MutableLiveData<ResultState<List<Post>>> = MutableLiveData()

    init {
        loadPosts()
    }

    private fun loadPosts() {
        postResult.postProgress()
        disposables += postDao.getPosts()
            .map(this::throwIfEmpty)
            .onErrorResumeNext(this::getDataFromApi)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = postResult::postSuccess,
                onError = postResult::postError
            )
    }

    private fun throwIfEmpty(list: List<Post>): List<Post> {
        if (list.isEmpty()) {
            throw Exception()
        }
        return list
    }

    private fun getDataFromApi(error: Throwable): Single<List<Post>> {
        return postApi.getPosts().map { apiPostList ->
            postDao.insertAll(apiPostList)
            apiPostList
        }
    }

    fun refresh() {
        loadPosts()
    }

    override fun onCleared() {
        disposables.clear()
    }
}